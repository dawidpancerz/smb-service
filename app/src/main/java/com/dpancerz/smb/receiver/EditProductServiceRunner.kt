package com.dpancerz.smb.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class EditProductServiceRunner : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
            context.startService(
                Intent(
                    context,
                    NotificationService::class.java
                )
            )
        }
    }
}