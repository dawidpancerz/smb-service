package com.dpancerz.smb.receiver

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var receiver: ProductBroadcastReceiver
    private lateinit var service: NotificationService
    @Volatile private var bound: Boolean = false
    private val connection = NotificationServiceConnection(
        onConnection = { connect(it) },
        onDisconnect = { disconnect() },
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        receiver = ProductBroadcastReceiver()
        receiver.createNotificationChannel(this)
    }

    override fun onStart() {
        super.onStart()

        // tylko na potrzeby testów:
        registerReceiver(receiver, IntentFilter("com.dpancerz.smb.products.NEW_PRODUCT")
            .also { it.priority = 555 }
        )

        val intent = Intent(this, NotificationService::class.java)
        bindService(intent, connection, BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        unbindService(connection)
        disconnect()
    }

    private fun connect(service: NotificationService) {
        this.service = service
        connected()
    }

    private fun connected() {
        bound = true
    }

    private fun disconnect() {
        bound = false
    }
}
