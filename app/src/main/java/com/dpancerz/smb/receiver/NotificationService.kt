package com.dpancerz.smb.receiver

import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Binder
import android.os.IBinder


class NotificationService : Service() {
    private lateinit var binder: IBinder

    init {
        this.binder = NSBinder()
    }

    inner class NSBinder : Binder() {
        val service: NotificationService = this@NotificationService
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder = binder
}

class NotificationServiceConnection(
    private val onConnection: (NotificationService) -> Unit,
    private val onDisconnect: () -> Unit
) : ServiceConnection {
    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        val nsBinder = service as (NotificationService.NSBinder)
        onConnection(nsBinder.service)
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        onDisconnect()
    }
}