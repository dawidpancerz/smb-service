package com.dpancerz.smb.receiver

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class ProductBroadcastReceiver : BroadcastReceiver() {
    companion object {
        const val channel = "product_notif"
        const val channelName = "Product Notification"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        extractNotification(intent)?.let { notification ->
            context?.let { ctx ->
                buildNotification(ctx, notification)
            }
        }
    }

    private fun extractNotification(intent: Intent?) = intent?.let {
        it.extras?.let { extras ->
            extras.get("id")?.let { id ->
                NewProductNotification(id = id as Long, name = extras.get("name")!! as String)
            }
        }
    }

    private fun buildNotification(context: Context, data: NewProductNotification) {
        val notificationManager = NotificationManagerCompat.from(context)
        val notification = NotificationCompat.Builder(context, channel)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("New product in store")
            .setContentText("Product ${data.name} added. Wanna look inside?")
            .setContentIntent(editProductIntentFor(context, data.id))
            .setAutoCancel(true)
            // others?
            .build()
        notificationManager.notify(data.id.toInt(), notification)
    }

    private fun editProductIntentFor(context: Context, productId: Long): PendingIntent? {
        val intent: Intent = context.packageManager
            ?.getLaunchIntentForPackage("com.dpancerz.smb.products")
            ?: throw IllegalStateException()
        with (intent) {
            action = Intent.ACTION_VIEW
            type = "text/plain"
            putExtra("ID", productId)
            removeCategory("android.intent.category.LAUNCHER")
            addCategory("android.intent.category.ALTERNATIVE")
            component = ComponentName(
                "com.dpancerz.smb.products",
                "com.dpancerz.smb.products.single.EditProductActivity"
            )
        }
        return PendingIntent.getActivity(
            context,
            12354,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_ONE_SHOT,
        )
    }

    fun createNotificationChannel(context: Context) {
        NotificationManagerCompat.from(context).createNotificationChannel(
            NotificationChannel(
                channel,
                channelName,
                NotificationManager.IMPORTANCE_DEFAULT
            )
        )
    }

}

data class NewProductNotification(
    val id: Long,
    val name: String
)
